var options = {
    item: 'degree-item'
};

var values = [
    // CAPPA
    {
        title: 'Architecture',
        description: 'Degrees offered in',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
        school: 'Architecture, Planning, and Public Affairs',
        
    },
    {
        title: 'City and Regional Planning',
        description: 'Degrees offered in',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Architecture, Planning, and Public Affairs',
        
    },
    {
        title: 'Interior Design',
        description: 'Degrees offered in',
        degreeLevels: '<span>Bachelors</span>',
        school: 'Architecture, Planning, and Public Affairs',
        
    },
    {
        title: 'Landscape Architecture',
        description: 'Degrees offered in',
        degreeLevels: '<span>Masters</span>',
        school: 'Architecture, Planning, and Public Affairs',
        
    },
    {
        title: 'Public Administration',
        description: 'Degrees offered in',
        degreeLevels: '<span>Masters</span> <span>Doctorate</span>',
        school: 'Architecture, Planning, and Public Affairs',
        
    },
    {
        title: 'Public Policy',
        description: 'Degrees offered in',
        degreeLevels: '<span>Masters</span> <span>Doctorate</span>',
        school: 'Architecture, Planning, and Public Affairs',
        
    },
    // Business
    {
        title: 'Accounting',
        description: 'Degrees offered in accouting, professional accounting, and taxation.',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Business',
        
    },
    {
        title: 'Business Administration',
        description: 'Degrees offered in business administration, executive business administration, international business administration, and real estate.',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Business',
        
    },
    {
        title: 'Economics',
        description: 'Degrees offered in economics.',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
        school: 'Business',
        
    },
    {
        title: 'Finance and Real Estate',
        description: 'Degrees offered in finance, finance (business administration), and quantitative finance.',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Business',
        
    },
    {
        title: 'Health Care Administration',
        description: 'Degrees offered in health care administration.',
        degreeLevels: '<span>Masters</span>',
        school: 'Business',
        
    },
    {
        title: 'Information Systems and Operation Management',
        description: 'Degrees offered in information systems, operations management, business analytics, and information systems (mathematical sciences).',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Business',
        
    },
    {
        title: 'Management',
        description: 'Degrees offered in management, human resource management, and management (business administration).',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Business',
        
    },
    {
        title: 'Marketing',
        description: 'Degrees offered in marketing, marketing research, and marketing (business administration).',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Business',
        
    },
    // Education
    {
        title: 'Curriculum & Instruction',
        description: 'Degrees offered in education curriculum & instruction, interdisciplinary studies (education), accelerated online education curriculum & instruction (mathematics, science, or triple literacy), mind, brain and education, reading specialist, teaching (early childhood, middle level or secondary level)',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
        school: 'Business',
        
    },
    {
        title: 'Educational Leadership & Policy Studies',
        description: 'Degrees offered in educational leadership & policy studies, educational leadership (K-16), and accelerated online education leadership and policy studies.',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Business',
        
    },
    // Engineering
    {
        title: 'Bioengineering',
        description: 'Degrees offered in biomedical engineering.',
        degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
        school: 'Engineering',
        
    },
];

var degreeItem = new ListeningStateChangedEvent('degree-list', options, values);