var options = {
	valueNames: [ 'title', 'description', 'degreeLevels', 'schoolName', { data: ['school'] }],
	item: '<li class="list--list-item" data-school ><h3 class="title"></h3><p class="description"></p><p class="degreeLevels"><span class="degreeLevels"></span></p><div class="schoolName"></div></li>',
	page: 10,
	pagination: true
};

var values = [
	// CAPPA
	{
		title: 'Architecture',
		description: 'Degrees offered in',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Architecture, Planning, and Public Affairs',
		school: 'architecture',
		titleLink: '#'
	},
	{
		title: 'City and Regional Planning',
		description: 'Degrees offered in',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Architecture, Planning, and Public Affairs',
		school: 'architecture',
		titleLink: '#'
	},
	{
		title: 'Interior Design',
		description: 'Degrees offered in',
		degreeLevels: '<span>Bachelors</span>',
		schoolName: 'Architecture, Planning, and Public Affairs',
		school: 'architecture',
		titleLink: '#'
	},
	{
		title: 'Landscape Architecture',
		description: 'Degrees offered in',
		degreeLevels: '<span>Masters</span>',
		schoolName: 'Architecture, Planning, and Public Affairs',
		school: 'architecture',
		titleLink: '#'
	},
	{
		title: 'Public Administration',
		description: 'Degrees offered in',
		degreeLevels: '<span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Architecture, Planning, and Public Affairs',
		school: 'architecture',
		titleLink: '#'
	},
	{
		title: 'Public Policy',
		description: 'Degrees offered in',
		degreeLevels: '<span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Architecture, Planning, and Public Affairs',
		school: 'architecture',
		titleLink: '#'
	},
	// Business
	{
		title: 'Accounting',
		description: 'Degrees offered in accouting, professional accounting, and taxation.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	{
		title: 'Business Administration',
		description: 'Degrees offered in business administration, executive business administration, international business administration, and real estate.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	{
		title: 'Economics',
		description: 'Degrees offered in economics.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	{
		title: 'Finance and Real Estate',
		description: 'Degrees offered in finance, finance (business administration), and quantitative finance.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	{
		title: 'Health Care Administration',
		description: 'Degrees offered in health care administration.',
		degreeLevels: '<span>Masters</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	{
		title: 'Information Systems and Operation Management',
		description: 'Degrees offered in information systems, operations management, business analytics, and information systems (mathematical sciences).',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	{
		title: 'Management',
		description: 'Degrees offered in management, human resource management, and management (business administration).',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	{
		title: 'Marketing',
		description: 'Degrees offered in marketing, marketing research, and marketing (business administration).',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Business',
		school: 'business',
		titleLink: '#'
	},
	// Education
	{
		title: 'Curriculum & Instruction',
		description: 'Degrees offered in education curriculum & instruction, interdisciplinary studies (education), accelerated online education curriculum & instruction (mathematics, science, or triple literacy), mind, brain and education, reading specialist, teaching (early childhood, middle level or secondary level)',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Education',
		school: 'education',
		titleLink: '#'
	},
	{
		title: 'Educational Leadership & Policy Studies',
		description: 'Degrees offered in educational leadership & policy studies, educational leadership (K-16), and accelerated online education leadership and policy studies.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Education',
		school: 'education',
		titleLink: '#'
	},
	// Engineering
	{
		title: 'Bioengineering',
		description: 'Degrees offered in biomedical engineering.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		school: 'Engineering',
		school: 'Engineering',
		titleLink: '#'
	},
	{
		title: 'Civil Engineering',
		description: 'Degrees offered in civil engineering.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Engineering',
		school: 'engineering',
		titleLink: '#'
	},
	{
		title: 'Computer Science and Engineering',
		description: 'Degrees offered in computer engineering, computer science, software engineering and computer science (mathematical sciences).',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Engineering',
		school: 'engineering',
		titleLink: '#'
	},
	{
		title: 'Electrical Engineering',
		description: 'Degrees offered in electrical engineering.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Engineering',
		school: 'engineering',
		titleLink: '#'
	},
	{
		title: 'Industrial and Manufacturing Systems Engineering',
		description: 'Degrees offered in industrial engineering, engineering management, logistics, and systems engineering.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Engineering',
		school: 'engineering',
		titleLink: '#'
	},
	{
		title: 'Materials Science and Engineering',
		description: 'Degrees offered in materials sceince and engineering.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Engineering',
		school: 'engineering',
		titleLink: '#'
	},
	{
		title: 'Mechanical and Aerospace Engineering',
		description: 'Degrees offered in aerospace engineering, and mechanical engineering.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Engineering',
		school: 'engineering',
		titleLink: '#'
	},
	// Liberal Arts
	{
		title: 'Sociology and Anthropology',
		description: 'Degrees offered in anthropology, and sociology.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Art & Art History',
		description: 'Degrees offered in art history, and art with areas of study in visual communications (graphic design, web design, app design, game design, and illustration), film and video, 3d studio, 2d studio, art education.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Communication',
		description: 'Degrees offered in advertising, broadcasting, communications, communication technology, journalism, public relations, communication studies with areas of focus in organizational and speech communication.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Criminology & Criminal Justice',
		description: 'Degrees offered in criminology & criminal justice.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'English',
		description: 'Degrees offered in english (available with minors in writing or creative writing), and english teaching.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'History',
		description: 'Degrees offered in history, history pre-law, history teaching, and transatlantic history.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Linguistics',
		description: 'Degrees offered in linguistics, and teaching english to speakers of other languages (TESOL).',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Modern Languages',
		description: 'Degrees offered in modern languages, french, spanish, teaching french or spanish, and critical languages and international studies.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Music',
		description: 'Degrees offered in with concentrationcs in all level band, choral/keyboard, choral/voice, orchestra, business, composition, instrumental orchestra and performance band, jazz studies, keyboard pedagogy, keyboard performance, media, theatre, theory, voice pedagogy, voice performance, and music performance.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Philosophy and Humanities',
		description: 'Degrees offered in philosophy (available with general or pre-professional tracks).',
		degreeLevels: '<span>Bachelors</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Political Science',
		description: 'Degrees offered in politcal science.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	{
		title: 'Theatre Arts',
		description: 'Degrees offered in theatre arts, and theatre arts teaching.',
		degreeLevels: '<span>Bachelors</span>',
		schoolName: 'Liberal Arts',
		school: 'liberalArts',
		titleLink: '#'
	},
	// Nursing
	{
		title: 'Nursing',
		description: 'Degrees offered in nursing, nurser practitioner, nursing administration, and nursing education.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Nursing and Health Innovation',
		school: 'health',
		titleLink: '#'
	},
	{
		title: 'Kinesiology',
		description: 'Degrees offered in kinesiology, exercise science and athletic training.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span>',
		schoolName: 'Nursing and Health Innovation',
		school: 'health',
		titleLink: '#'
	},
	// Science
	{
		title: 'Biology',
		description: 'Degrees offered in biology, biology teaching, medical technology, microbiology, biology, and quantitative biology.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Science',
		school: 'science',
		titleLink: '#'
	},
	{
		title: 'Chemistry & Biochemistry',
		description: 'Degrees offered in biochemistry, biological chemistry, chemistry, and chemistry teaching.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Science',
		school: 'science',
		titleLink: '#'
	},
	{
		title: 'Earth and Environmental Sciences',
		description: 'Degrees offered in earth and environmental science, geoinformatics, geology, and geology teaching.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Science',
		school: 'science',
		titleLink: '#'
	},
	{
		title: 'Interdisciplinary Science',
		description: 'Degrees offered in interdisciplinary science.',
		degreeLevels: '<span>Masters</span>',
		schoolName: 'Science',
		school: 'science',
		titleLink: '#'
	},
	{
		title: 'Mathematics',
		description: 'Degrees offered in mathematics, mathematics teaching.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Science',
		school: 'science',
		titleLink: '#'
	},
	{
		title: 'Physics',
		description: 'Degrees offered in physics, physics teaching, and applied physics.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Science',
		school: 'science',
		titleLink: '#'
	},
	{
		title: 'Psychology',
		description: 'Degrees offered in psychology with available concentrations in experimental, health/neurscience, and industrial and organizational.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Science',
		school: 'science',
		titleLink: '#'
	},
	// Social Work
	{
		title: 'Social Work',
		description: 'Degrees offered in social work.',
		degreeLevels: '<span>Bachelors</span> <span>Masters</span> <span>Doctorate</span>',
		schoolName: 'Social Work',
		school: 'socialWork',
		titleLink: '#'
	}
];

var userList = new List('major-list', options, values);
var values_school = [];
var values_degreeLevels = [];
var values_degreeLevelString = "";

function resetList(){
	values_school = [];
	values_degreeLevels = [];
	userList.search();
	userList.filter();
	userList.update();
	$('.filter-all').prop('checked', true);
	$('.filter').prop('checked', false);
	$('.search').val('');
	//console.log('Reset Successfully!');
};
  
// function updateList(){
// 	var values_school = $('input[name=school]:checked').val();
// 	var values_degreeLevels = $('input[name=degreeLevels]:checked').val();
// 	console.log(values_school, values_degreeLevels);

// 	userList.filter(function (item) {
// 		var schoolFilter = false;
// 		var degreeLevels = false;
		
// 		if(values_school == null)
// 		{ 
// 			schoolFilter = true;
// 		} else {
// 			//schoolFilter = item.values().school == values_school;
// 			schoolFilter = item.values().school.indexOf(values_school) >= 0;
// 		}

// 		if(values_degreeLevels == null)
// 		{ 
// 			degreeLevels = true;
// 		} else {
// 			degreeLevels = item.values().degreeLevels.indexOf(values_degreeLevels) >= 0;
// 		}

// 		return degreeLevels && schoolFilter
// 	});
	
// 	userList.update();
// 	//console.log('Filtered: ' + values_school);
// }


//filtering for stackable filters
$('.filter').change(function() {
	var isChecked = this.checked;
	var value = $(this).val();

	var schoolFilter = false;
var degreeLevels = false;
	
	if($(this).attr('name') == 'degreeLevels') {
		if(isChecked){
			//  add to list of active filters
			values_degreeLevels.push(value);
			// console.log(values_degreeLevels);
		}
		else
		{
			// remove from active filters
			values_degreeLevels.splice(values_degreeLevels.indexOf(value), 1);
		}
	} else {
		if(isChecked){
			//  add to list of active filters
			values_school.push(value);
			// console.log(values_school);
		}
		else
		{
			// remove from active filters
			values_school.splice(values_school.indexOf(value), 1);
		}
	}

	userList.filter(function (item) {
		var schoolFilter = false;
		var degreeLevels = false;
		
		if(values_school.length == 0)
		{ 
			schoolFilter = true;
		} else {
			schoolFilter = values_school.indexOf(item.values().school) >= 0;
		}

		if(values_degreeLevels.length == 0)
		{ 
			degreeLevels = true;
		} else {
			$(values_degreeLevels).each(function(index, value) {
				degreeLevels = item.values().degreeLevels.indexOf(value) >= 0;
			});
		}

		return degreeLevels && schoolFilter;
	});

	userList.update();


    // userList.filter(function (item) {
    //     if(values_school.length > 0 )
    //     {
	// 		return(values_school.indexOf(item.values().school)) > -1;
	// 	}
		
	// 	if(values_degreeLevels.length > 0)
    //     {
	// 		return(values_degreeLevels.indexOf(item.values().degreeLevels)) > -1;
	// 	}

    //     return true;
    // });
});



//Modify for stackable filters and when all fields are unchecked!! - 10/16
$(function(){
	//updateList();
	// $('input[name=school]').change(updateList);
	// $('input[name=degreeLevels]').change(updateList);
	
	userList.on('updated', function (list) {
		if (list.matchingItems.length > 0) {
			$('.no-result').hide()
		} else {
			$('.no-result').show()
		}
	});
});



//Highlight word during live search
